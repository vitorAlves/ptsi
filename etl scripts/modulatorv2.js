var async = require('async');
const fs = require('fs')
let results = {
	"nodes": [],
	"links": []
}
let ids = [];
const years = [1993, 1994/* , 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,  2018*/]
async.series({
	processing: function (callback) {
		for (const year of years) {
			let obj = JSON.parse(fs.readFileSync('' + year + '_merged.json', 'utf-8'))
			obj.forEach(reg => {
				let groups = [];
				async.series({
					group: function (callback) {
						reg.fos.forEach(group => {
							groups.push(group.name);
						});
						callback(null);
					},
					node: function (callback) {
						let node = {
							id: parseInt(reg.id),
							group: groups,
							year: parseInt(reg.year),
							title: reg.title,
							authors: reg.authors,
							category: reg.primary_cat.split(".")[0],
							n_citation: reg.n_citations,
						};
						callback(null, node);
					},
					link: function (callback) {
						reg.references.forEach(element => {
							results.links.push({
								"source": parseInt(reg.id),
								"target": parseInt(element),
								"value": 1
							});
						});
						callback(null);
					}
				}, function (err, res) {
					results.nodes.push(res.node);
				});
			});
		};
		callback(null)
	},
	prunning: function (callback) {
		async.series({
			picking: function (callback) {
				results.nodes.forEach(node => {
					ids.push(node.id);
				});
				callback(null);
			},
			cutting: function (callback) {
				results.links = results.links.filter(filterout);
				callback(null);
			}
		}, function (err, res) {
			// meh
		});
		callback(null, results)
	}
}, function (err, res) {
	fs.appendFileSync('paulo3.json', JSON.stringify(res.prunning));
	console.log(res)
});

function filterout(obj) {
	return ids.includes(obj.target)
}