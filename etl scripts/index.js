const csv = require('csv-parser')
const fs = require('fs')
const results = [];

//merges multiple outputs by year
/* const years = [ 1993,1994, 1995, 1996, 1997,1998,1999,2000] */
const years = [ 2019]
for (const year of years){
/* years.forEach(year => { */

	let obj = JSON.parse(fs.readFileSync('relation_data/' + year + '.txt', 'utf-8'))
	fs.createReadStream('paper_data/' + year + '.tsv')
		.pipe(csv({
			separator: '\t'
		}))
		.on('data', (data) => results.push(data))
		.on('end', () => {
			results.forEach(result => {
				obj.forEach(element => {
					if (result.title == element.title && element.n_citation>0 &&  element.references &&  element.references.length) {
						fs.appendFileSync(year+'_merged.json', JSON.stringify(Object.assign(element, result)) + ",\n");
					}
				});
			});
		});
		console.log("year "+year+" doing");
}/* ) */;
