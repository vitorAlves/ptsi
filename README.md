# JUNCTION - Projeto PTSI/IVISSEM

Na sequência de trabalhos na área das Altmétricas sociais dirigidas a Investigadores surgiu a necessidade de implementar visualizações

Para correr este projecto 
```sh
npm i
npm start
```
Visitar 
 - localhost:3000 - para o force directed graph
 - localhost:3000/paulo - para o radial graph

 As configurações localizam-se em bin/www

## force directed graph
Em relação à transformação de dados foram usados os seguintes datasets:

 - https://www.kaggle.com/tayorm/arxiv-papers-metadata
 - https://www.aminer.cn/aminer_data?fbclid=IwAR21dQ2UcaTVos0BD-ZZrFEsAHBbaTYfKjpd7c2JI_iHafRWlNYdhiVHbc8

Os quais foram transformados com recurso aos scripts na pasta etl scripts e alguma edição manual(i.e. regex).
O carregamento para a base de dados Mongo é baseado no script upload.js.


## radial graph

Em relação à geração de dados foi usada a seguinte estrutura:
```json
 {
     "name": "",
     "children": [{
             "name": "Engineering and Technology",
             "colour": "#4B3CE0",
             "children": [{
                     "id": 100062,
                     "name": "consequat pariatur aliquip dolore elit",
                     "year": 2017,
                     "authors": "Ruiz Lawrence, Cantrell Pittman",
                     "keywords": "occaecat et aliqua mollit irure",
                     "DOI": "22a97a55-3cb9-4962-9e61-40d93f60a280",
                     "link": "541a6003-0308-41a7-afac-603f8fcd9dc9"
                 }
             ]
         },
         {
             "name": "Health",
             "colour": "#85B44D",
             "children": [{
                     "id": 100068,
                     "name": "amet do aute dolore aliquip",
                     "year": 1999,
                     "authors": "Jeannie Singleton, Vonda Marsh",
                     "keywords": "deserunt mollit esse anim ea",
                     "DOI": "1e7d3de6-3b2b-4237-836b-d808118f5900",
                     "link": "3edaa6ec-87ac-4889-8940-45773ebf6ca8"
                 },
                 {
                     "id": 100069,
                     "name": "minim anim sunt incididunt est",
                     "year": 2001,
                     "authors": "Eddie Larson, Myers Cameron",
                     "keywords": "dolore in voluptate do ipsum",
                     "DOI": "d47b2ec4-e35c-4250-b069-133e68cee947",
                     "link": "235844e0-677c-4843-8745-f730e92d5d0c"
                 }
             ]
         },
         {
             "name": "users",
             "children": [{
                 "name": "users",
                 "children": [{
                    "id": 1000000,
                    "name": "Tanisha Aguilar",
                    "profile": "dce2b378-0907-4354-93fe-40fde06986bb"
                  },
                  {
                    "id": 1000001,
                    "name": "Corinne Miranda",
                    "profile": "85289ae7-3390-4321-b68a-84f8cf2792cb"
                  }
                 ]
             }]
         }
     ],

     "linkList": [
        {
          "source": 10000052,
          "target": 100082,
          "thickness": 1
        },
        {
          "source": 10000047,
          "target": 100065,
          "thickness": 2
        },
        {
          "source": 10000044,
          "target": 100073,
          "thickness": 3
        }
      ]
 }
```

com o gerador em https://next.json-generator.com/
 - links
```json
[
  {
    'repeat(95, 100)': {
      source: '100000{{integer(0, 84)}}',
      target: '1000{{integer(0, 95)}}',
        thickness: '{{random(1, 2, 3)}}',
  
    }
  }
]
```
 - Investigadores
```json
[
  {
    'repeat(77, 77)': {
      id: '100000{{index()}}',
      name: '{{firstName()}} {{surname()}}',
        profile: '{{guid()}}'
    }
  }
]
```
 - Papers
```json
[
  {
    'repeat(85, 100)': {
      id: '1000{{index()}}',
      name: '{{lorem(5, "words")}}',
      year: '{{integer(1994, 2020)}}',
      authors: '{{firstName()}} {{surname()}}, {{firstName()}} {{surname()}}',
      keywords: '{{lorem(5, "words")}}',
      DOI: '{{guid()}}',
        link: '{{guid()}}'
    }
  }
]
```



## Integração
Dada a falta de referencias deixamos a integração algo em aberto
 - Esta pode ser feira com a integração das rotas, ejs, entre outros numa outra (ou nesta) aplicação baseada em EXPRESS.js
 - Pode ser feita por ajax (i.e. ``` $( "#result" ).load( "ajax/test.html" ); ```) da div que contem a visualização
 - Pode ser colocada uma *iframe* na página 












