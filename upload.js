let json;
let fs = require('fs');
const mongoose = require('mongoose');


const uri = "mongodb+srv://ptsi:DamnB0yeH3Thick@cluster0-fg2sg.mongodb.net/ptsi?retryWrites=true&w=majority";
mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(() => {
    console.log('MongoDB Connected…')
    parse();
  })
  .catch(err => console.log(err))


const link = mongoose.Schema({
  source: Number,
  target: Number
});


const Link = mongoose.model('Links', link);

const paper = mongoose.Schema({
  title: String,
  year: Number,
  id: Number,
  doi: String,
  authors: [String],
  n_citation: Number,
  primary_cat: String,
  references: [String]
});


const Paper = mongoose.model('Papers', paper);

let parse = () => {
  fs.readdir('./etl', function (err, files) {
    for (file of files) {
      console.log(file);
      json = require(`./etl/${file}`);
      let uploadFile;
      for (e of json) {
        let authors = [];
        for (author of e.authors) {
          authors.push(author.name);
        }
        uploadFile = {
          id: e.id,
          title: e.title,
          year: e.year,
          authors: authors,
          n_citation: e.n_citation,
          doi: e.doi,
          primary_cat: e.primary_cat.split(".")[0],
          references: e.references,
        }

        const upload = new Paper(uploadFile);

        upload.save(function (err) {
          if (err) return handleError(err);
        })

      }

    }

  });
}

let parseLinks = () => {
  fs.readdir('./etl', function (err, files) {
      for (file of files) {
        console.log(file);
        json = require(`./etl/${file}`);
        let uploadLink;

        for (let e of json) {

          uploadLink = [];
          let index = 0;
          for (let reference of e.references) {
            index += 1;
            uploadLink.push({source: parseInt(e.id, 10), target: parseInt(reference)});
            if (index >= e.references.length) {
              uploadFunc(uploadLink)
            }
          }

          function uploadFunc(uploadLink) {
           Link.collection.insert(uploadLink, onInsert);
            function onInsert(err, docs) {
              if (err) {
                console.log(err);
              } else {
                console.info('%d potatoes were successfully stored.', docs.length);
              }
            }

          }

        }

      }

    }
  )
  ;
}
