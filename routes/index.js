var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const request = require('request');


const uri = "mongodb+srv://ptsi:DamnB0yeH3Thick@cluster0-fg2sg.mongodb.net/ptsi?retryWrites=true&w=majority";
mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(() => {
    console.log('MongoDB Connected…')
  })
  .catch(err => console.log(err))

var paper = mongoose.Schema({
  title: String,
  year: Number,
  id: Number,
  authors: [String],
  n_citation: Number,
  primary_cat: String,
  references: [String]
});


var Paper = mongoose.model('Papers', paper);
/* GET home page. */


const link = mongoose.Schema({
  source: Number,
  target: Number
});


const Link = mongoose.model('Links', link);

router.get('/', function (req, res, next) {
  let callLinks = (id, callback) => {
    Link.find({'source': {$in: id}, 'target': {$in: id}}, function (err, links) {
      callback(links);

    });
  }

  Paper.find().sort({'n_citation': 'desc'}).limit(70).exec(function (err, papers) {
      if (err) return console.log(err);

      let finalYears = [];
      let finalCats = [];
      let id = [];
      let finalNodes = [];
      let missingIds = [];
      let index = 0;

      Paper.distinct("year").exec(function (err, years) {
        finalYears = years
      })
      Paper.distinct("primary_cat").exec(function (err, cats) {
        finalCats = cats
      })

      for (var e of papers) {
        index += 1;
        id.push((e.id));
        let checkReferences = (index, e) => {
          if (index >= papers.length) {
            Paper.find({'id': {$in: missingIds}}).exec(function (err, missingPapers) {
              callLinks(id, (links) => {
                console.log(papers.length);
                res.render('index', {
                  links: JSON.stringify(links),
                  years: JSON.stringify(finalYears),
                  cats: finalCats,
                  nodes: JSON.stringify(papers),
                  title: 'Express'
                });
              })
            })
          }
        }
        checkReferences(index, e);
      }
    }
  );
});


router.post('/', function (req, res, next) {
  let min = req.body.min;
  let max = req.body.max;
  let categories = req.body['categories[]'];
  let backYears = req.body.backYears;

  let callLinks = (id, callback) => {
    Link.find({'source': {$in: id}, 'target': {$in: id}}, function (err, links) {
      callback(links);
    });
  }

  let id = [];
  let finalNodes = [];
  let missingIds = [];
  let missingFunc = (missingIds, papers, backYears) => {
    let year = min - 1 - backYears;
    console.log(missingIds, year);
    Paper.find({
      $and: [{'id': {$in: missingIds}}, {
        'year': {$gt: year}
      }, {'primary_cat': {$in: categories}}]
    }).exec(function (err, missingPapers) {

      let indexer = 0;
      for (var m of missingPapers) {
        indexer++;
        id.push((m.id));
        m.references = [];
        console.log(m);
        if (indexer >= missingPapers.length) {
          papers = papers.concat(missingPapers);
          callLinks(id, (links) => {
            return res.json({
              links: links,
              nodes: papers,
              status: 200
            });
          })
        }
      }
    });
  }

  let nonMissing = (papers) => {
    let indexer = 0;
    indexer++;
    callLinks(id, (links) => {
      return res.json({
        links: links,
        nodes: papers,
        status: 200
      });
    })
  }
  console.log(categories);
  Paper.find({
    $and: [{
      'year': {$gt: min - 1, $lt: max + 1}
    }, {'primary_cat': {$in: categories}}]
  }).sort({"n_citation": "desc"}).limit(80).exec(function (err, papers) {
      if (err) return console.log(err);

      let index = 0;

      for (var e of papers) {
        index += 1;
        id.push((e.id));

        let checkReferences = (index, e) => {
          let ind = 0;
          for (let r of e.references) {
            ind++;
            let exists = papers.findIndex(x => x.id == parseInt(r));
            if (exists > 0) {
            } else {
              missingIds.push(parseInt(r));
            }

            if (index >= papers.length && ind >= e.references.length) {

              if (backYears < 1) {
                nonMissing(papers);
              } else {
                missingFunc(missingIds, papers, backYears);
              }

            }
          }
        }
        checkReferences(index, e);
      }
    }
  );

});


router.get('/paulo', function (req, res, next) {
 res.render('paulo', {

    title: 'Express'
  });
});
module.exports = router;

