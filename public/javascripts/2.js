// URL: https://observablehq.com/@pauloandrade/radial-bilevel-graph/2
// Title: Radial Bilevel Graph
// Author: Paulo Andrade (@pauloandrade)
// Version: 774
// Runtime version: 1

const m0 = {
  id: "5d57dffde1a5370e@774",
  variables: [
    {
      inputs: ["md"],
      value: (function (md) {
        return (
          md`# Radial Bilevel Graph
Custom graph v5.6`
        )
      })
    },
    {
      name: "chart",
      inputs: ["tree", "data", "d3", "colornone", "event", "color", "k", "autoBox"],
      value: (function (tree, data, d3, colornone, event, color, k, autoBox) {
          const root = tree(data);
          console.log(root.data);
          const svg = d3.create("svg");

          function getlinknode(linktobechecked, callback) {
            let b = {};
            var ctr = 0;
            root.leaves().forEach(function (nodeslctd, index, array) {
              ctr++;
              // console.log(nodeslctd.data)
              if (linktobechecked == nodeslctd.data.id) {
                b = {
                  "x": nodeslctd.x,
                  "y": nodeslctd.y
                };
              }
              if (ctr === array.length) {
                callback(b)
              }
            })
          }

          var linkews = root.data.linkList;

          //console.log(data.links);
          function wrap(text, width) {
            text.each(function () {
              var text = d3.select(this),
                words = text.text().split(/\s+/).reverse(),
                word,
                line = [],
                lineNumber = 0,
                lineHeight = 1.1,
                y = text.attr("y"),
                dy = parseFloat(text.attr("dy")),
                tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em")
              while (word = words.pop()) {
                line.push(word)
                tspan.text(line.join(" "))
                if (tspan.node().getComputedTextLength() > width) {
                  line.pop()
                  tspan.text(line.join(" "))
                  line = [word]
                  tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", `${++lineNumber * lineHeight + dy}em`).text(word)
                }
              }
            })
          }

          function getlinks() {
            let linkews4 = []
            linkews.forEach(function (l, index) {
              getlinknode(l.target, (source) => {

                getlinknode(l.source, (target) => {
                  //console.log(target)
                  linkews4.push({
                    "source": {
                      "x": target.x,
                      "y": target.y,
                      "oh": l.target
                    },
                    "target": {
                      "x": source.x,
                      "y": source.y,
                      "oh": l.source
                    },

                    "thickness": l.thickness
                  })
                })
              })
            });
            return linkews4
          }

          svg.append("g")
            .attr("fill", "none")
            .style("mix-blend-mode", "darken")
            .attr("stroke", colornone)

            .attr("stroke-opacity", 0.4)

            .selectAll("path")
            //.data(root.links())
            .data(getlinks())
            .attr("stroke-width", 5)// (d => d.data.thickness))
            .join("path")
            .attr("d", d3.linkRadial()
              .angle(d => d.x)
              .radius(d => d.y))
            .attr("class", (d => d.oh))
            .attr("class", "rlink")
            .attr("stroke-width", (d => d.thickness))
            .attr(("class", (d => d.oh)));
          svg.append("g")
            .selectAll("circle")
            .data(root.leaves())
            .join("circle")
            .attr("transform", d => `
        rotate(${d.x * 180 / Math.PI - 90})
        translate(${d.y},0)
      `)
            .attr("fill", (d => d.parent.data.colour))
            .attr("data", (d => JSON.stringify(d.data)))
            // .attr("id", (d => d.data.id))
            //.attr("r", 5)
            .attr("r", d => d.depth == 2 ? 5 : 0)
            //.on("mouseover", function(){return tooltip.style("visibility", handleMouseOverCircle()+"visible")})
            .on("mousemove", function () {
              return tooltip.style("top", (event.pageY - 10) + "px").style("left", (event.pageX + 10) + "px");
            })
            //	.on("mouseout", function(){return tooltip.style("visibility", "hidden"+handleMouseOutCircle);})
            .on("mouseover", handleMouseOverCircle)
            .on("mouseout", handleMouseOutCircle)
            //.attr("id", (d => d.id)
            .attr("depth", (d => d.depth))
            .attr("id", (d => d.data.id))
          var div = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);
          var tooltip = d3.select("body")
            .append("div")
            .style("position", "absolute")
            .style("z-index", "10")
            .style("visibility", "hidden")
            .text("a simple tooltip");

          function handleMouseOverCircle(d, i) {
            createHighlights(d3.select(this).attr("id"));
            d3.select(this).style("r", d => d.depth == 2 ? 8 : 0);
            console.log(d3.select(this).attr("data"))
            console.log(d3.select(this).attr("id"))
            tooltip.style("visibility", "visible")
            // .attr("r", d => d.depth == 2 ? 5 : 0)
            //    d3.select(this).style("fill", "#000");
            /*  div.transition()
                         .duration(200)
                         .style("opacity", .9);
                    div	.html( "benis<br/>" )
                      .style("left", (d3.event.pageX) + "px")
                       .style("top", (d3.event.pageY - 28) + "px");*/
          }

          function handleMouseOutCircle(d, i) {
            /* div.transition()
                        .duration(500)
                        .style("opacity", 0);*/
            d3.selectAll(".temp").remove()
            d3.select(this).style("r", d => d.depth == 2 ? 5 : 0);
            tooltip.style("visibility", "hidden" + handleMouseOutCircle);
          }

          function handleMouseOver(d, i) {
            createHighlights(d3.select(this).attr("id"));
            d3.select(this).style("fill", "#000");
            // console.log((d3.select("temp").attr("data")))
          }

          var links2 = svg
            .selectAll('links')

          function createHighlights(id) {
            svg.append("g")
              .attr("fill", "none")
              .style("mix-blend-mode", "darken")
              .attr("stroke", (d, i) => color(d3.easeQuad(i / ((1 << k) - 1))))
              .attr("stroke-opacity", 0.4)
              .attr("stroke-width", 2.5)
              .selectAll("path")
              .data(getlinks().filter(hasId))
              .join("path")
              .attr("d", d3.linkRadial()
                .angle(d => d.x)
                .radius(d => d.y))
              .attr("class", "temp")
              .attr("stroke-width", (d => d.thickness * 1.2))

            function hasId(input) {
              if (input.source.oh == id || input.target.oh == id) {
                return input
              }
              //console.log(getlinks().filter(hasId))
              //return getlinks().filter(hasId)
            }
          }

          function unhighlightAll() {
            d3.selectAll(".temph").remove()
            //  console.log("exit");
          }

          function highlightAll() {

            console.log("hersh");
            svg.append("g")
              .attr("fill", "none")
              .style("mix-blend-mode", "darken")
              .attr("stroke", (d, i) => color(d3.easeQuad(i / ((1 << k) - 1))))
              .attr("stroke-opacity", 0.4)
              .attr("stroke-width", 2.5)
              .selectAll("path")
              .data(getlinks())
              .join("path")
              .attr("d", d3.linkRadial()
                .angle(d => d.x)
                .radius(d => d.y))
              .attr("class", "temph")


          }

          function handleMouseOut(d, i) {
            d3.selectAll(".temp").remove()
            d3.select(this).style("fill", "#555");
          }

          // console.log(svg.selectAll("links"))
          svg.append("g")
            .attr("font-family", "sans-serif")
            .attr("font-size", 15)
            .attr("stroke-linejoin", "round")
            .attr("stroke-width", 3)
            .selectAll("text")
            .data(root.leaves())
            .join("text")
            .attr("transform", d => `
        rotate(${d.x * 180 / Math.PI - 90})
        translate(${d.y},0)
        rotate(${d.x >= Math.PI ? 180 : 0})
      `)
            .attr("dy", "0.31em")
            .attr("class", "tags")//+(d => d.depth))
            .attr("fill", d => d.depth == 3 ? "#555" : "#ffffffff")
            .attr("font-size", d => d.depth == 3 ? 15 : 0)
            //.attr("font-size", 15)
            .on("mouseover", handleMouseOver)
            .on("mouseout", handleMouseOut)
            .attr("x", d => d.x < Math.PI === !d.children ? 6 : -6)
            .attr("depth", (d => d.depth))
            .attr("id", (d => d.data.id))
            .attr("text-anchor", d => d.x < Math.PI === !d.children ? "start" : "end")
            .text(d => d.data.name)
            //  .call(wrap, 20)
            .clone(true).lower()
            .attr("stroke", "white");
          //  svg.selectAll("text")
          // .call(wrap, 20)
          let bb = 200
          svg.call(d3.zoom()
            //.extent([0-bb,0-bb],[bb,bb])
            .scaleExtent([1, 8])
            .on("zoom", zoomed));


          svg.selectAll(".rlink")

            .on("mouseout", unhighlightAll())
          //  .on("mouseover", highlightAll())
          svg.selectAll(".tags")
          //   .attr("stroke", "#ddd")
          //.attr("font-size", 15)
          // d3.select('svg').on('mousedown.zoom',null);
          // svg.on("mousedown", null)
          function zoomed() {
            svg.attr("transform", d3.event.transform);
          }

          return svg.attr("viewBox", autoBox).node();
        }
      )
    },
    {
      name: "autoBox",
      value: (function () {
        return (
          function autoBox() {
            document.body.appendChild(this);
            const {x, y, width, height} = this.getBBox();
            document.body.removeChild(this);
            return [x, y, width, height];
          }
        )
      })
    },
    {
      name: "autoBoxZoom",
      value: (function () {
        return (
          function autoBoxZoom() {
            document.body.appendChild(this);
            const {x, y, width, height} = this.getBBox();
            document.body.removeChild(this);
            return [[x, y], [width, height]];
          }
        )
      })
    },
    {
      name: "data",
      inputs: ["d3", "FileAttachment"],
      value: (async function (d3, FileAttachment) {
        return (
          d3.hierarchy(await FileAttachment("new@11.json").json())
            .sort((a, b) => d3.descending(a.data.name, b.data.name))
        )
      })
    },
    {
      name: "tree",
      inputs: ["d3", "radius"],
      value: (function (d3, radius) {
        return (
          d3.tree()
            .size([2 * Math.PI, radius * 1.05])
            .separation((a, b) => ((a.depth < 2 && b.depth < 2 || a.name == "users" || b.name == "users") ? 0 : ((a.parent == b.parent ? 2 : 3)) / (a.depth * 2.2)))
        )
      })
    },
    {
      name: "height",
      value: (function () {
        return (
          1954
        )
      })
    },
    {
      name: "width",
      value: (function () {
        return (
          954
        )
      })
    },
    {
      name: "radius",
      inputs: ["width"],
      value: (function (width) {
        return (
          width / 2
        )
      })
    },
    {
      name: "d3",
      inputs: ["require"],
      value: (function (require) {
        return (
          require("d3@5")
        )
      })
    },
    {
      name: "color",
      inputs: ["d3"],
      value: (function (d3) {
        return (
          t => d3.interpolateSpectral(1 - t)
        )
      })
    },
    {
      name: "k",
      value: (function () {
        return (
          2
        )
      })
    },
    {
      name: "colornone",
      value: (function () {
        return (
          "#ccc"
        )
      })
    },
    {
      name: "colorout",
      value: (function () {
        return (
          "#f00"
        )
      })
    },
    {
      name: "colorin",
      value: (function () {
        return (
          "#00f"
        )
      })
    },
    {}
  ]
};

const notebook = {
  id: "5d57dffde1a5370e@774",
  modules: [m0]
};

export default notebook;
