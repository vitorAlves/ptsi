// https://observablehq.com/@pauloandrade/radial-bilevel-graph/2@774
export default function define(runtime, observer) {
	const main = runtime.module();
	const fileAttachments = new Map([
		["new@11.json", new URL("./files/20306c1e69cc0bbfa99c5e67fc6bab3474c9c32dd133bf99ec13a6dcc7f7c7c1e31c40148080157ee2280c4c3dea46c82b42564bd4a917376aa303daf7ba7b6b",
			import.meta.url)]
	]);
	main.builtin("FileAttachment", runtime.fileAttachments(name => fileAttachments.get(name)));
	main.variable(observer()).define(["md"], function (md) {
		return (
			md `# Radial Bilevel Graph
Custom graph v5.6`
		)
	});
	main.variable(observer("chart")).define("chart", ["tree", "data", "d3", "colornone", "color", "k", "autoBox"], function (tree, data, d3, colornone, color, k, autoBox) {
		const root = tree(data);
		console.log(root.data);
		const svg = d3.create("svg");
		function getlinknode(linktobechecked, callback) {
			let b = {};
			var ctr = 0;
			root.leaves().forEach(function (nodeslctd, index, array) {
				ctr++;
				// console.log(nodeslctd.data)
				if (linktobechecked == nodeslctd.data.id) {
					b = {
						"x": nodeslctd.x,
						"y": nodeslctd.y
					};
				}
				if (ctr === array.length) {
					callback(b)
				}
			})
		}
		var linkews = root.data.linkList;
		//console.log(data.links);
		function wrap(text, width) {
			text.each(function () {
				var text = d3.select(this),
					words = text.text().split(/\s+/).reverse(),
					word,
					line = [],
					lineNumber = 0,
					lineHeight = 1.1,
					y = text.attr("y"),
					dy = parseFloat(text.attr("dy")),
					tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em")
				while (word = words.pop()) {
					line.push(word)
					tspan.text(line.join(" "))
					if (tspan.node().getComputedTextLength() > width) {
						line.pop()
						tspan.text(line.join(" "))
						line = [word]
						tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", `${++lineNumber * lineHeight + dy}em`).text(word)
					}
				}
			})
		}
		function getlinks() {
			let linkews4 = []
			linkews.forEach(function (l, index) {
				getlinknode(l.target, (source) => {
					getlinknode(l.source, (target) => {
						//console.log(target)
						linkews4.push({
							"source": {
								"x": target.x,
								"y": target.y,
								"oh": l.target
							},
							"target": {
								"x": source.x,
								"y": source.y,
								"oh": l.source
							},
							"thickness": l.thickness
						})
					})
				})
			});
			return linkews4
		}
		svg.append("g")
			.attr("fill", "none")
			.style("mix-blend-mode", "darken")
			.attr("stroke", colornone)
			.attr("stroke-opacity", 0.4)
			.selectAll("path")
			//.data(root.links())
			.data(getlinks())
			.attr("stroke-width", 5) // (d => d.data.thickness))
			.join("path")
			.attr("d", d3.linkRadial()
				.angle(d => d.x)
				.radius(d => d.y))
			.attr("class", (d => d.oh))
			.attr("class", "rlink")
			.attr("stroke-width", (d => d.thickness))
			.attr(("class", (d => d.oh)));
		svg.append("g")
			.selectAll("circle")
			.data(root.leaves())
			.join("circle")
			.attr("class", "circle")
			.attr("transform", d => `
        rotate(${d.x * 180 / Math.PI - 90})
        translate(${d.y},0)
      `)
			.attr("fill", (d => d.parent.data.colour))
			.attr("area", (d => d.parent.data.name))
			.attr("data", (d => JSON.stringify(d.data)))
			// .attr("id", (d => d.data.id))
			//.attr("r", 5)
			.attr("r", d => d.depth == 2 ? 5 : 0)
			//.on("mouseover", function(){return tooltip.style("visibility", handleMouseOverCircle()+"visible")})
			.on("mousemove", function () {
				return tooltip.style("top", (event.pageY - 10) + "px").style("left", (event.pageX + 10) + "px");
			})
			//	.on("mouseout", function(){return tooltip.style("visibility", "hidden"+handleMouseOutCircle);})
			.on("mouseover", handleMouseOverCircle)
			.on("mouseout", handleMouseOutCircle)
			//.attr("id", (d => d.id)
			.attr("depth", (d => d.depth))
			.attr("id", (d => d.data.id))
		var div = d3.select("body").append("div")
			.attr("class", "tooltip")
			.style("opacity", 0);
		var tooltip = d3.select(".tooltipi")
			.style("position", "absolute")
			//.attr("class", "tooltipi")
			.style("z-index", "10")
			.style("visibility", "hidden")
		//	.text("a simple tooltip");
		function handleMouseOverCircle(d, i) {
			createHighlights(d3.select(this).attr("id"));
			d3.select(this).style("border", d => d.depth == 2 ? "2px solid" : "none");
			console.log(d3.select(this).attr("data"))
			console.log(d3.select(this).attr("id"))
			tooltip.attr("class", "active")
			tooltip.style("visibility", "visible")
			d3.select(this).style("r", d => d.depth == 2 ? 8 : 0)
			tooltip.style("border-color", d3.select(this).attr("fill"))
			//  tooltip.text(d3.select(this).attr("data"))
			var current = JSON.parse(d3.select(this).attr("data"))
			$('#fillTitle').text(current.name);
			$('#fillCat').text(d3.select(this).attr("area"));
			$('#fillYear').text(current.year);
			$('#fillAuthor').text(current.authors);
			$('#fillKeywords').text(current.keywords);
			$('#fillDOI').text(current.DOI);
		}
		function handleMouseOutCircle(d, i) {
			d3.selectAll(".circle").style("r", d => d.depth == 2 ? 5 : 0)
			d3.selectAll(".temp").remove()
			d3.select(this).style("border", d => d.depth == 2 ? "0px solid" : "none");
			tooltip.attr("class", "")
		}
		function handleMouseOver(d, i) {
			createHighlights(d3.select(this).attr("id"));
			d3.select(this).style("fill", "#000");
		}
		var links2 = svg
			.selectAll('links')
		function createHighlights(id) {
			svg.append("g")
				.attr("fill", "none")
				.style("mix-blend-mode", "darken")
				.attr("stroke", (d, i) => color(d3.easeQuad(i / ((1 << k) - 1))))
				.attr("stroke-opacity", 0.4)
				.attr("stroke-width", 2.5)
				.selectAll("path")
				.data(getlinks().filter(hasId))
				.join("path")
				.attr("d", d3.linkRadial()
					.angle(d => d.x)
					.radius(d => d.y))
				.attr("class", "temp")
				.attr("stroke-width", (d => d.thickness * 1.2))
			function hasId(input) {
				if (input.source.oh == id || input.target.oh == id) {
					return input
				}
			}
		}
		var toggle = true
		$(".observablehq").on("contextmenu", function (e) {
			if (toggle) {
				highlightAll()
				toggle = !toggle
			} else {
				unhighlightAll()
				toggle = !toggle
			}
			// debugger;
			return false;
		});
		function unhighlightAll() {
			d3.selectAll(".temph").remove()
		}
		function highlightAll() {
			console.log("hersh");
			svg.append("g")
				.attr("fill", "none")
				.style("mix-blend-mode", "darken")
				.attr("stroke", (d, i) => color(d3.easeQuad(i / ((1 << k) - 1))))
				.attr("stroke-opacity", 0.4)
				.attr("stroke-width", 2.5)
				.selectAll("path")
				.data(getlinks())
				.join("path")
				.attr("d", d3.linkRadial()
					.angle(d => d.x)
					.radius(d => d.y))
				.attr("class", "temph")
		}
		function handleMouseOut(d, i) {
			d3.selectAll(".temp").remove()
			d3.select(this).style("fill", "#555");
		}
		svg.append("g")
			.attr("font-family", "sans-serif")
			.attr("font-size", 15)
			.attr("stroke-linejoin", "round")
			.attr("stroke-width", 3)
			.selectAll("text")
			.data(root.leaves())
			.join("text")
			.attr("transform", d => `
        rotate(${d.x * 180 / Math.PI - 90})
        translate(${d.y},0)
        rotate(${d.x >= Math.PI ? 180 : 0})
      `)
			.attr("dy", "0.31em")
			.attr("class", "tags")
			.attr("fill", d => d.depth == 3 ? "#555" : "#ffffffff")
			.attr("font-size", d => d.depth == 3 ? 15 : 0)
			//.attr("font-size", 15)
			.on("mouseover", handleMouseOver)
			.on("mouseout", handleMouseOut)
			.attr("x", d => d.x < Math.PI === !d.children ? 6 : -6)
			.attr("depth", (d => d.depth))
			.attr("id", (d => d.data.id))
			.attr("text-anchor", d => d.x < Math.PI === !d.children ? "start" : "end")
			.text(d => d.data.name)
			//  .call(wrap, 20)
			.clone(true).lower()
			.attr("stroke", "white");
		let bb = 200
		svg.call(d3.zoom()
			//.extent([0-bb,0-bb],[bb,bb])
			.scaleExtent([0.8, 8])
			.on("zoom", zoomed));
		svg.selectAll(".rlink")
			.on("mouseout", unhighlightAll())
		//  .on("mouseover", highlightAll())
		svg.selectAll(".tags")
		//   .attr("stroke", "#ddd")
		//.attr("font-size", 15)
		// d3.select('svg').on('mousedown.zoom',null);
		// svg.on("mousedown", null)
		function zoomed() {
			svg.attr("transform", d3.event.transform);
		}
		return svg.attr("viewBox", autoBox).node();
	});
	main.variable(observer("autoBox")).define("autoBox", function () {
		return (
			function autoBox() {
				document.body.appendChild(this);
				const {
					x,
					y,
					width,
					height
				} = this.getBBox();
				document.body.removeChild(this);
				return [x, y, width, height];
			}
		)
	});
	main.variable(observer("autoBoxZoom")).define("autoBoxZoom", function () {
		return (
			function autoBoxZoom() {
				document.body.appendChild(this);
				const {
					x,
					y,
					width,
					height
				} = this.getBBox();
				document.body.removeChild(this);
				return [
					[x, y],
					[width, height]
				];
			}
		)
	});
	main.variable(observer("data")).define("data", ["d3", "FileAttachment"], async function (d3, FileAttachment) {
		return (
			d3.hierarchy(await FileAttachment("new@11.json").json())
			.sort((a, b) => d3.descending(a.data.name, b.data.name))
		)
	});
	main.variable(observer("tree")).define("tree", ["d3", "radius"], function (d3, radius) {
		return (
			d3.tree()
			.size([2 * Math.PI, radius * 1.05])
			.separation((a, b) => ((a.depth < 2 && b.depth < 2 || a.name == "users" || b.name == "users") ? 0 : ((a.parent == b.parent ? 2 : 3)) / (a.depth * 2.2)))
		)
	});
	main.variable(observer("height")).define("height", function () {
		return (
			1954
		)
	});
	main.variable(observer("width")).define("width", function () {
		return (
			954
		)
	});
	main.variable(observer("radius")).define("radius", ["width"], function (width) {
		return (
			width / 2
		)
	});
	main.variable(observer("d3")).define("d3", ["require"], function (require) {
		return (
			require("d3@5")
		)
	});
	main.variable(observer("color")).define("color", ["d3"], function (d3) {
		return (
			t => d3.interpolateSpectral(1 - t)
		)
	});
	main.variable(observer("k")).define("k", function () {
		return (
			2
		)
	});
	main.variable(observer("colornone")).define("colornone", function () {
		return (
			"#ccc"
		)
	});
	main.variable(observer("colorout")).define("colorout", function () {
		return (
			"#f00"
		)
	});
	main.variable(observer("colorin")).define("colorin", function () {
		return (
			"#00f"
		)
	});
	return main;
}